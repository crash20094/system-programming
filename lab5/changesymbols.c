﻿
#include <string.h>
#include <ctype.h>
#include "stdio.h"
#include "conio.h"


int strAnalis(char* str)
{
	int counter = 0;
	while (*str) 
	{
		if (*str == 40) 
		{
			*str=(char)60;
		}
		else if (*str == 41)
		{
			*str = (char)62;
		}
		else if (((*str >= 65) && (*str <= 90)) || ((*str >= 97) && (*str <= 122)))
		{
			*str = (char)toupper(*str);
			counter++;
		}
		++str;
	}
	return counter;
}

int main()
{
	char st[256];
	int counter = 0;
	puts("Input string ");
	if (fgets(st, 256, stdin) == NULL) return -1;
	int l = strlen(st);
	counter=strAnalis(st);
	printf("Letter count: %d, Non-letter count:%d, new string: %s", counter,(l-counter-1), st);
	_fgetchar();
	return 0;
}
