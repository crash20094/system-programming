/*
*-------Примеры уравнений-----------
*отрицательный дискриминант: 2 4 7;
*положительный дискриминант: 2 4 -7;
*дисрименант нулевой: 1 6 9;
*-----------------------------------
*/

int disrc_men(int a,int b,int c)
{
    double D=((pow(b,2))-4*a*c);
  if(D<0)
  {
    return 1;
  }
}

int disrc_Bol(int a,int b,int c,float *x1, float *x2)
{
  double D=((b*b)-4*a*c);//36-4*1*9
  printf("Discriminant: %f\n",D);
  if(D>0)
  {
    *x1=((-b)+sqrtf(D))/(2*a);
    *x2=((-b)-sqrtf(D))/(2*a);
    return 1;
  }
  else if(D==0) {*x1=((-b)+sqrtf(D))/(2*a); return 0;}
  else if(disrc_men(a,b,c)) {return -1;}
}

float x1;
float x2;
int main()
{
int a,b,c={0};

printf("Enter a,b,c \n");
scanf("%d %d %d",&a,&b,&c);
int res=disrc_Bol( a, b,c, &x1, &x2);
switch(res)
{
  case -1:{printf("Material roots are not present! \n");break;}
  case 0:{printf("Both roots of the equation are equal:%f \n",x1);break;}
  case 1:{printf("Equation roots are equal:%f,%f \n",x1,x2);break;}

}
return 0;
}
